# README
## 軟實 AS -01. Web Canvas -- 105062101 郭庭均

### Function Describe
- window.onload() 
	- 網頁一 load 進來就會先執行這個 function，在這個 function 中呼叫 initButtons() 來初始化所有 button，並初始化畫布。
- initButtons()
	- 將所有的 button 在這個 function 中 addEventListener，其中包含畫筆、橡皮擦、畫筆size、文字、重新整理、挑選顏色等。

- mousePosition() & mouseMove()
	- 在這個 function 中會追蹤滑鼠移動的軌跡，作為畫筆、橡皮擦等運作時的依據。

- getTextSizeFont()
	- 處理 text input 時的字型及字體大小。
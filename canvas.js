var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext('2d');
var mode;
var typeText = "abc";
var brushSize = "medium";

var x = 0;
var y = 0;
var brushBtn = document.querySelector("#brushBtn");
var eraserBtn = document.querySelector("#eraserBtn");
var textBtn = document.querySelector("#textBtn");
var textInput = document.querySelector("#inputdefault");
var textSize = document.querySelector("#textSize");
var textFont = document.querySelector("#textFont");
var refreshBtn = document.querySelector("#refreshBtn");
var pickColorBtn = document.querySelector("#pickColorBtn");
var xsmallBtn = document.querySelector("#xsmallBtn");
var smallBtn = document.querySelector("#smallBtn");
var mediumBtn = document.querySelector("#mediumBtn");
var largeBtn = document.querySelector("#largeBtn");

window.onload = function() {
    initButtons();
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
};

function initButtons() {
    brushBtn.addEventListener("click", function() {
        mode = this.id;
        ctx.strokeStyle = pickColorBtn.style.backgroundColor;
        canvas.style.cursor = "url(brush.png)";
    });
    eraserBtn.addEventListener("click", function() {
        mode = this.id;
        ctx.strokeStyle = "white";
        
    });
    xsmallBtn.addEventListener("click", function() {
        brushSize = "small";
        ctx.lineWidth = 1;
    });
    smallBtn.addEventListener("click", function() {
        brushSize = "small";
        ctx.lineWidth = 5;
    });
    mediumBtn.addEventListener("click", function() {
        brushSize = "medium";
        ctx.lineWidth = 10;
    });
    largeBtn.addEventListener("click", function() {
        brushSize = "large";
        ctx.lineWidth = 15;
    });
    textBtn.addEventListener("click", function() {
        mode = this.id;
        getTextSizeFont();
        ctx.textAlign = "center";
        ctx.fillStyle = "red";
        ctx.fillText(textInput.value, canvas.width/2, canvas.height/2);
    });
    refreshBtn.addEventListener("click", function() {
        mode = this.id;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
    });

    pickColorBtn.addEventListener("click", function() {
        mode = "brushBtn";
        ctx.strokeStyle = pickColorBtn.style.backgroundColor;
    });

    $("#saveBtn").click(function() {
        var Url = canvas.toDataURL();
        this.href = Url;
    });
}

canvas.addEventListener('mousedown', function(evt) {
    var mousePos = mousePosition(canvas, evt);
    evt.preventDefault();
    if (mode == "eraserBtn") ctx.strokeStyle = "white";
    else ctx.strokeStyle = pickColorBtn.style.backgroundColor;
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);  
    canvas.addEventListener('mousemove', mouseMove, false);
});

canvas.addEventListener('mouseup', function() {
    canvas.removeEventListener('mousemove', mouseMove, false);
}, false);

function mousePosition(canvas, evt) {
    var size = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - size.left,
        y: evt.clientY - size.top
    };
}

function mouseMove(evt) {
    var mousePos = mousePosition(canvas, evt);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
}

function getTextSizeFont() {
    var tmpFont;

    if (textSize.value == "12px") tmpFont = "12px";
    else if (textSize.value == "16px") tmpFont = "16px";
    else if (textSize.value == "20px") tmpFont = "20px";
    else if (textSize.value == "32px") tmpFont = "32px";
    else tmpFont = "16px";

    if (textFont.value == "Arial") tmpFont = tmpFont + " Arial";
    else if (textFont.value == "Helvetica Neue") tmpFont = tmpFont + " Helvetica Neue";
    else if (textFont.value == "sans-serif") tmpFont = tmpFont + " sans-serif";
    else tmpFont = tmpFont + " Helvetica Neue";

    ctx.font = tmpFont;
}